**POSTMAN API testing tool:**
Postman is a standalone software testing API (Application Programming Interface) platform to build, test, design, modify, and document APIs.

**Pre-Requisites:**
1. Postman Application 
2. Node.js
3. NPM (Node package manager)
4. Newman 

**command:**
1. Install Postman Application - "https://www.postman.com/downloads/"
2. Install nodejs - "install nodejs"
   (nodejs --version (Mac) - checking installed version) 
3. Install NPM - "npm install -g npm"
   (npm --version - checking installed version)
4. Install Newman "npm install -g newman”
    OR
   Install Newman using Homebrew "brew install newman" 
   (newman --version (Mac) - checking installed version)
   
**Run Collection in Local using Newman:**
"newman run mycollection.json"
OR
"newman run "Collection Link"
OR
"newman run mycollection.json -n 10" # runs the collection 10 times